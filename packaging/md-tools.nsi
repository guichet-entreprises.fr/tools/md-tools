;##############################################################################
; @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
; 	All Rights Reserved.
; 	Unauthorized copying of this file, via any medium is strictly prohibited
; 	Dissemination of this information or reproduction of this material
; 	is strictly forbidden unless prior written permission is obtained
; 	from Guichet Entreprises.
;##############################################################################
;---------------------------------
;  General
;---------------------------------
!addincludedir "./nsh"
!include "MUI2.nsh"
!include "StrFunc.nsh"
!include "fileassoc.nsh"

;---------------------------------
; The product
;---------------------------------
!define PRODUCT_SHORTNAME 	"md-tools"
!define PRODUCT_LONGNAME 	"Markdown tools"
!define PRODUCT_VERSION 	"1.4"

!define BN_PKG "${PRODUCT_SHORTNAME}"
!include "build_number_increment.nsh"
!include "context_menu.nsh"

;---------------------------------
; Explorer context and registry
;---------------------------------
!define DESCRIPTION 		"Markdown File"
!define MENU_DESCRIPTION 	"Markdown Tools"
!define EXT 			"md"
!define FILECLASS 		"markdown.file"

;---------------------------------
; General
;---------------------------------
!define /date TIMESTAMP "%Y-%m-%d"

;---------------------------------
Name "${PRODUCT_LONGNAME}"
OutFile "../setup_${PRODUCT_SHORTNAME}-v${PRODUCT_VERSION}-[${BUILD_NUMBER}]-${TIMESTAMP}.exe"
ShowInstDetails "nevershow"
ShowUninstDetails "nevershow"
CRCCheck On

;---------------------------------
!define MUI_ICON "icon/ge.ico"
!define MUI_UNICON "icon/ge.ico"
BrandingText "Guichet Entreprises - ${TIMESTAMP}"

;--------------------------------
;Folder selection page
InstallDir "$PROGRAMFILES\ge.fr\${PRODUCT_SHORTNAME}"
InstallDirRegKey HKCU "Software\${PRODUCT_SHORTNAME}" ""

;--------------------------------
;Modern UI Configuration
 
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages
;--------------------------------
!insertmacro MUI_LANGUAGE "French"

;-------------------------------- 
;Installer Sections     
Section "install"  
	;Add files
	SetOutPath "$INSTDIR"

 	File /r /x *.log "..\\distribution\\*.*"
 	File /r /x *.log "icon\\*.ico"
	
	SetOutPath "$INSTDIR\MarkdownEdit"
 	File /r "..\\third_party_software\\MarkdownEdit\\*.*"
	SetOutPath "$INSTDIR\MarkText"
 	File /r "..\\third_party_software\\MarkText\\*.*"
	
	DeleteRegKey HKCU `SOFTWARE\Classes\.${EXT}`
	DeleteRegKey HKCU `SOFTWARE\Classes\${FILECLASS}`
	DeleteRegKey HKCU `Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.${EXT}`
	
	DeleteRegKey HKCR `${FILECLASS}`
	DeleteRegKey HKCR `.${EXT}`
	
	WriteRegStr 		HKCR ".${EXT}" "" "${FILECLASS}"
	WriteRegStr 		HKCR "${FILECLASS}" "" `${DESCRIPTION}`
 	WriteRegStr 		HKCR "${FILECLASS}\DefaultIcon" "" `$INSTDIR\md.ico`
	WriteRegExpandStr 	HKCR "${FILECLASS}\shell\Open\command" "" `"$INSTDIR\MarkdownEdit\mde.exe" "%1"`
	
	!insertmacro ADD_CONTEXT_MENU "md" `Edit with Mark Text` `"$INSTDIR\MarkText\Mark Text.exe" "%1"` `"$INSTDIR\ge.ico"` `"$INSTDIR\marktext.ico"`
	!insertmacro ADD_CONTEXT_MENU "md" `Edit with MarkdownEdit` `"$INSTDIR\MarkdownEdit\mde.exe" "%1"` `"$INSTDIR\ge.ico"` `$INSTDIR\MarkdownEdit\mde.exe,0`
	!insertmacro ADD_CONTEXT_MENU "md" `Correct` `"$INSTDIR\md-tools.exe" --correct-md=yes "%1"` `"$INSTDIR\ge.ico"` `"$INSTDIR\repair.ico"`
	!insertmacro ADD_CONTEXT_MENU "md" `Normalize` `"$INSTDIR\md-tools.exe" --normalize-md=yes "%1"` `"$INSTDIR\ge.ico"` `"$INSTDIR\normalize.ico"`
	!insertmacro ADD_CONTEXT_MENU "md" `Convert to PDF` `"$INSTDIR\md-tools.exe" --conv-pdf=yes "%1"` `"$INSTDIR\ge.ico"` `"$INSTDIR\pdf.ico"`
	!insertmacro ADD_CONTEXT_MENU "md" `Include references` `"$INSTDIR\md-tools.exe" --include-refs=yes --include-refs-depth-up=1 --include-refs-depth-down=-1 "%1"` `"$INSTDIR\ge.ico"` `"$INSTDIR\include.ico"`
	

	!insertmacro UPDATEFILEASSOC
	
	WriteUninstaller "$INSTDIR\Uninstall.exe"
 
SectionEnd
 
 
;--------------------------------    
;Uninstaller Section  
Section "un.Uninstall"
 
	;Delete Files 
	RMDir /r "$INSTDIR\*.*"    

	;Remove the installation directory
	RMDir "$INSTDIR"


	!insertmacro DEL_CONTEXT_MENU "md" `Edit with Mark Text` 
	!insertmacro DEL_CONTEXT_MENU "md" `Correct` 
	!insertmacro DEL_CONTEXT_MENU "md" `Normalize`
	!insertmacro DEL_CONTEXT_MENU "md" `Convert to PDF`
	!insertmacro DEL_CONTEXT_MENU "md" `Include references`


	DeleteRegKey HKCU `SOFTWARE\Classes\.${EXT}`
	DeleteRegKey HKCU `SOFTWARE\Classes\${FILECLASS}`
	DeleteRegKey HKCU `Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.${EXT}`
	
	DeleteRegKey HKCR `${FILECLASS}`
	DeleteRegKey HKCR `.${EXT}`

SectionEnd
