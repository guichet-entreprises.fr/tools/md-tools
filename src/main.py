﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

###############################################################################
# @package mdtools
# Markdown Tools develops for Gucihet Entreprises
#
###############################################################################

import logging
import sys
import os
import traceback
import argparse
import tempfile
import ctypes  # An included library with Python install.

import pymdtools.common as common
from normalize import correct_markdown_file
from pymdtools import markdown_file_beautifier
from pymdtools import convert_md_to_pdf
from pymdtools import search_include_refs_to_md_file

__actions_list__ = {}
__actions_list__['correct_markdown_file'] = correct_markdown_file
__actions_list__['markdown_file_beautifier'] = markdown_file_beautifier
__actions_list__['convert_md_to_pdf'] = convert_md_to_pdf
__actions_list__['search_include_refs_to_md_file'] =\
    search_include_refs_to_md_file

###############################################################################
# NullStream behaves like a stream but does nothing.
###############################################################################
class NullStream:
    def __init__(self):
        pass

    def write(self, data):
        pass

    def read(self, data):
        pass

    def flush(self):
        pass

    def close(self):
        pass

###############################################################################
# Storage for stream
###############################################################################
class InOutStream:
    def __init__(self, stream=NullStream()):
        self.stdout = stream
        self.stderr = stream
        self.stdin = stream
        self.__stdout__ = stream
        self.__stderr__ = stream
        self.__stdin__ = stream

    def save_current_stream(self):
        self.stdout = sys.stdout
        self.stderr = sys.stderr
        self.stdin = sys.stdin
        self.__stdout__ = sys.__stdout__
        self.__stderr__ = sys.__stderr__
        self.__stdin__ = sys.__stdin__

    def apply_to_std_stream(self):
        sys.stdout = self.stdout
        sys.stderr = self.stderr
        sys.stdin = self.stdin
        sys.__stdout__ = self.__stdout__
        sys.__stderr__ = self.__stderr__
        sys.__stdin__ = self.__stdin__

###############################################################################
# Retrieve the initial stream
###############################################################################
@common.static(__stream__=None)
def initial_stream():
    if initial_stream.__stream__ is None:
        initial_stream.__stream__ = InOutStream()
        initial_stream.__stream__.save_current_stream()
    return initial_stream.__stream__

###############################################################################
# Test the frozen situation of the executable
###############################################################################
def is_frozen():
    return getattr(sys, 'frozen', False)


###############################################################################
# Change the std stream in the frozen case
###############################################################################
if is_frozen():
    initial_stream().save_current_stream()
    InOutStream(NullStream()).apply_to_std_stream()

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""
    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__
    return result

###############################################################################
# Create a windows message box
#
# @param text The message
# @param title The title of the windows
# @return nothing.
###############################################################################
def message_box(text, title):
    ctypes.windll.user32.MessageBoxW(0, text, title, 0)


###############################################################################
# Define the parsing of arguments of the command line
###############################################################################
def get_parser_for_command_line():
    docstring = ""
    for action in __actions_list__:
        docstring = docstring + action + ":\n" + \
            __actions_list__[action].__doc__.split("@")[0][0:-3] + "\n\n"

    description = \
        """This program take a list of markdown file(s)
and could apply one action.

""" + docstring

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--correct-md', action='store', dest='correct_md',
                        choices=['yes', 'no'], default='no',
                        help='Correct the MD file')
    parser.add_argument('--normalize-md', action='store', dest='normalize_md',
                        choices=['yes', 'no'], default='no',
                        help='Normalize the MD file')
    parser.add_argument('--conv-pdf', action='store', dest='conv_pdf',
                        choices=['yes', 'no'], default='no',
                        help='Convert to PDF')
    parser.add_argument('--include-refs', action='store', dest='include_ref',
                        choices=['yes', 'no'], default='no',
                        help='Include reference from other files')
    parser.add_argument('--include-refs-depth-up', action='store',
                        dest='include_ref_depth_up',
                        type=int, default=1,
                        help='Number of upper folder to search '
                        'for files and find references.')
    parser.add_argument('--include-refs-depth-down', action='store',
                        dest='include_ref_depth_down',
                        type=int, default=-1,
                        help='Number of deeper folder to search '
                        'for files and find references')
    parser.add_argument('--backup', action='store', dest='backup',
                        choices=['yes', 'no'], default='yes',
                        help='Define if the orginal file is backup.')
    parser.add_argument('--ext', metavar="'.ext'", action='store', dest='ext',
                        default='.md',
                        help='markdown filename extension definition.')
    parser.add_argument('--windows', action='store', dest='windows',
                        choices=['yes', 'no'], default='yes',
                        help='Define if we need all popups windows.')
    parser.add_argument('--verbose', action='store', dest='verbose',
                        choices=['yes', 'no'], default='no',
                        help='Put the logging system on the console for info.')
    parser.add_argument('--console', action='store', dest='console',
                        choices=['yes', 'no'], default='no',
                        help='Set the output to the standard output '
                        'for console')
    parser.add_argument('filenames', metavar='filename',
                        nargs='+',
                        help='list of filenames.')

    return parser


###############################################################################
# Logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'

    if is_frozen():
        log_filename = os.path.abspath(os.path.join(
            tempfile.gettempdir(),
            os.path.basename(__get_this_filename()) + '.log'))

    logging.basicConfig(filename=log_filename, level=logging.INFO,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler(initial_stream().stdout)
    console.setLevel(logging.ERROR)

    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger

    if not is_frozen():
        logging.getLogger('').addHandler(console)

    return console


###############################################################################
# Main script
###############################################################################
def __main():
    console = __set_logging_system()
    # ------------------------------------
    logging.info('+')
    logging.info('-------------------------------------------------------->>')
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    try:
        parser = get_parser_for_command_line()
        logging.info("parsing args")
        args = parser.parse_args()
        logging.info("parsing done")
        if args.verbose == "yes":
            console.setLevel(logging.INFO)
        if args.console == "yes":
            initial_stream().apply_to_std_stream()
            if is_frozen():
                logging.getLogger('').addHandler(console)

        args.backup_bool = (args.backup == "yes")
        args.windows_bool = (args.windows == "yes")
        #  args.check_filename_bool 	= (args.check_filename 		== "yes")
        args.correct_md_bool = (args.correct_md == "yes")
        args.normalize_md_bool = (args.normalize_md == "yes")
        #  args.conv_html_bool 		= (args.conv_html 		== "yes")
        args.conv_pdf_bool = (args.conv_pdf == "yes")
        args.include_ref_bool = (args.include_ref == "yes")
        #  args.conv_chm_bool 		= (args.conv_chm 		== "yes")

        logging.info("backup=%s", args.backup)
        logging.info("verbose=%s", args.verbose)
        logging.info("console=%s", args.console)
        logging.info("ext=%s", args.ext)
        logging.info("filenames=%s", repr(args.filenames))

        #  logging.info("check-filename=%s"%(args.check_filename))
        logging.info("correct-md=%s", args.correct_md)
        logging.info("normalize-md=%s", args.normalize_md)
        #  logging.info("conv-html=%s"%(args.conv_html))
        logging.info("conv-pdf=%s", args.conv_pdf)
        #  logging.info("conv-chm=%s"%(args.conv_chm))
        logging.info("include_ref_depth_up=%s", args.include_ref_depth_up)
        logging.info("include_ref_depth_down=%s",
                     args.include_ref_depth_down)

        count = 1
        max_count = len(args.filenames)

        error_msg = []

        for filename in args.filenames:
            logging.info(">>> Working on %03d / %03d : %s",
                         count, max_count, filename)
            count = count + 1
            try:
                local_filename = filename
                #  if ('args' in locals()) and (args.check_filename_bool):
                #  local_filename = os.path.join(os.path.dirname(filename),
                #           check_markdown_filename(local_filename,
                #                             filename_ext = args.ext))
                if ('args' in locals()) and (args.correct_md_bool):
                    correct_markdown_file(
                        local_filename, backup_option=args.backup_bool,
                        filename_ext=args.ext)
                if ('args' in locals()) and (args.normalize_md_bool):
                    markdown_file_beautifier(
                        local_filename, backup_option=args.backup_bool,
                        filename_ext=args.ext)
                #  if ('args' in locals()) and (args.conv_html_bool):
                    #  convert_md_to_html(local_filename,
                    #                     filename_ext = args.ext)
                if ('args' in locals()) and (args.conv_pdf_bool):
                    convert_md_to_pdf(local_filename, filename_ext=args.ext)
                #  if ('args' in locals()) and (args.conv_chm_bool):
                    #  convert_md_to_chm(local_filename,
                    #                        filename_ext = args.ext)
                if ('args' in locals()) and (args.include_ref_bool):
                    search_include_refs_to_md_file(
                        local_filename, backup_option=args.backup_bool,
                        filename_ext=args.ext,
                        depth_up=args.include_ref_depth_up,
                        depth_down=args.include_ref_depth_down)

            except Exception as ex:
                var = traceback.format_exc()
                logging.error('Unknown error : \n', var)
                # accumulate all error
                error_msg.append((filename, str(ex)))

        if len(error_msg) > 0:
            msg = "The following error(s) appends "\
                "during all the process : \r\n\r\n"
            for err in error_msg:
                msg = msg + \
                    "For filename=%s the following error append '%s'\r\n" % (
                        err[0], err[1])
            msg = msg + "\n\n No others errors appends on others files.\r\n"
            raise Exception(msg)

    except argparse.ArgumentError as errmsg:
        logging.error(str(errmsg))
        if ('args' in locals()) and (args.windows_bool):
            message_box(text=parser.format_usage(), title='Usage')

    except SystemExit:
        if ('args' in locals()) and (args.windows_bool):
            message_box(text=parser.format_help(), title='Help')

    except Exception as ex:
        logging.error(str(ex))
        if ('args' in locals()) and (args.windows_bool):
            message_box(text=str(ex), title='Usage')

    except:
        var = traceback.format_exc()
        logging.error('Unknown error : %s\n', var)
        if ('args' in locals()) and (args.windows_bool):
            message_box(text='Unknown error : \n' + var,
                        title='Error in this program')
        # raise

    logging.info('Finished')
    logging.info('<<--------------------------------------------------------')
    logging.info('+')
    # ------------------------------------


# -----------------------------------------------------------------------------
# Call main if the script is main
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    __main()
